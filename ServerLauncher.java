

import java.rmi.*;
import java.rmi.server.*;

import ChatApp.DataBase;
import ChatApp.Server;

import java.rmi.registry.*;
public class ServerLauncher {

  public static void  main(String [] args) {
	  try {
		  /*
		  // Create a Hello remote object
	    HelloImpl h = new HelloImpl ("Hello world !");
	    Hello h_stub = (Hello) UnicastRemoteObject.exportObject(h, 0);

	    // Register the remote object in RMI registry with a given identifier
	    Registry registry= LocateRegistry.getRegistry();
	    registry.rebind("HelloService2000", h_stub);
*/
		  
		DataBase dataBase = new DataBase();
		Server server = (Server) UnicastRemoteObject.exportObject(dataBase, 0);
	    Registry registry= LocateRegistry.getRegistry();
	    registry.rebind("Server", server);

	    System.out.println ("Server ready");


	  } catch (Exception e) {
		  System.err.println("Error on server :" + e) ;
		  e.printStackTrace();
	  }
  }
}
