package ChatApp;

public class ChatRoomDataExtended extends ChatRoomData{
	private static final long serialVersionUID = 1L;
	ChatRoomStatus status;
	public ChatRoomDataExtended(ChatRoomData data, ChatRoomStatus status) {
		super(data.getID(), data.getName(), data.getOwner());
		this.status = status;
	}
	
	public ChatRoomStatus getStatus() {
		return status;
	}

}
