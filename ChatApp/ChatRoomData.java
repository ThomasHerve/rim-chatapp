package ChatApp;

public class ChatRoomData implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	protected Integer ID;
	protected String name;
	protected UserData owner;
	public ChatRoomData(Integer ID, String name,UserData owner) {
		this.ID = ID;
		this.name = name;
		this.owner= owner;
	}
	
	public Integer getID() {
		return ID;
	}
	
	public  void setOwner(UserData owner) {
		this.owner= owner;
	}
	
	public  UserData getOwner() {
		return owner;
	}
	public String getName() {
		return name;
	}
	
	public void setName(String newName) {
		name = newName;
	}
}
