package ChatApp;

import java.rmi.RemoteException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Date;
public class DataBase implements Server{
	private List<User> users;//la liste des personnes enregistrés
	private List<ChatRoom> chatRooms;
	private int chatRoomID;
	private Connection c = null;
    private Statement stmt = null;
	public DataBase(){
		
		users = new ArrayList<>();
		chatRooms = new ArrayList<>();
		chatRoomID = 0;
		createBD();
		LoadBD();
	}
	
	private void createBD() {	      
	      try {
	         Class.forName("org.sqlite.JDBC");
	         c = DriverManager.getConnection("jdbc:sqlite:chat.db");
	         String sql;
	         //DROP
		  /* stmt = c.createStatement();
	         sql = "DROP TABLE CHATROOM ";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         stmt = c.createStatement();
	         sql = "DROP TABLE USERS ";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         stmt = c.createStatement();
	         sql = "DROP TABLE MEMBERS ";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         
	         stmt = c.createStatement();
	         sql = "DROP TABLE MESSAGES ";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         
	         stmt = c.createStatement();
	         sql = "DROP TABLE ASKJOIN ";
	         stmt.executeUpdate(sql);
	         stmt.close();*/
	         try { 
	        	 stmt = c.createStatement();
	        	 sql = "CREATE TABLE USERS " +
	                        "(LOGIN String PRIMARY KEY     NOT NULL," +
	                        "NAME String NOT NULL,"+
	                        " PASSWORD INTEGER   NOT NULL,"+
	                         "TOKEN INTEGER NOT NULL)";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         }catch ( Exception e ) {
	        	 
	         }
	         try {
	         stmt = c.createStatement();
	         sql = "CREATE TABLE CHATROOM " +
	                        "(ID INT PRIMARY KEY     NOT NULL," +
	                        " NAME           CHAR(50)   NOT NULL,"
	                        + "OWNER String NOT NULL)";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         
	         }catch ( Exception e ) {
	        	 
	         }
	         try {
	         stmt = c.createStatement();
	         sql = "CREATE TABLE MEMBERS " +
	                        "(IDUSER STRING NOT NULL," +
	                        "IDROOM  INTEGER   NOT NULL)";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         }catch ( Exception e ) {
	         }
	         try {
	         stmt = c.createStatement();
	         sql = "CREATE TABLE MESSAGES " +
	                        "(TIME varchar(10) PRIMARY KEY     NOT NULL," +
	                        "IDROOM  INTEGER   NOT NULL, "+
	                        "IDUSER String NOT NULL,"+
	                        "CONTENT CHAR(500))";
	         stmt.executeUpdate(sql);
	         stmt.close();
	         }catch ( Exception e ) {
	        	 
	         }
	         try {
		         stmt = c.createStatement();
		         sql = "CREATE TABLE ASKJOIN " +
		                        "(IDUSER STRING NOT NULL," +
		                        "IDROOM  INTEGER   NOT NULL)";
		         stmt.executeUpdate(sql);
		         stmt.close();
		         }catch ( Exception e ) {
		         }
	      } catch ( Exception e ) {
	         System.err.println( e.getClass().getName() + ": " + e.getMessage() );
	         System.exit(0);
	      }
	      System.out.println("Opened database successfully");
	      System.out.println("Table created successfully");
	}
	
	private void LoadBD() {
	      try {
			stmt = c.createStatement();	
	      ResultSet rs = stmt.executeQuery( "SELECT * FROM USERS;" );
	      while ( rs.next() ) {
	         users.add(new User(rs.getString("LOGIN"),rs.getString("NAME"),rs.getInt("PASSWORD"),rs.getInt("TOKEN")));
	         System.out.println( "ID = " + rs.getString("LOGIN"));
	         System.out.println( "NAME = " + rs.getString("NAME"));
	         System.out.println( "PWD = " + rs.getString("PASSWORD"));
	         System.out.println( "TOKEN = " + rs.getString("TOKEN"));
	         System.out.println();
	      }
	      rs.close();
	      stmt.close();
	      
			stmt = c.createStatement();	
		      rs = stmt.executeQuery( "SELECT * FROM CHATROOM;" );
		      while ( rs.next() ) {
		    	  chatRooms.add(new ChatRoom(getUserFromID(rs.getString("OWNER")),rs.getInt("ID"),rs.getString("NAME"))); 
		    	 System.out.println( "ow = " + rs.getString("OWNER"));
		         System.out.println( "NAME = " + rs.getString("NAME"));
		         System.out.println( "ID = " + rs.getInt("ID"));
		         System.out.println();
		         if (chatRoomID <=rs.getInt("ID")) {
		        	 chatRoomID=rs.getInt("ID")+1;
		         }
		      }
		      rs.close();
		      stmt.close();
		      
				stmt = c.createStatement();	
			      rs = stmt.executeQuery( "SELECT * FROM MEMBERS;" );
			      while ( rs.next() ) {
			    	 ChatRoom c = getChatRoomFromID(rs.getInt("IDROOM"));
			    	 c.addUser(getUserFromID(rs.getString("IDUSER")));
			         System.out.println( "USER = " + rs.getString("IDUSER"));
			         System.out.println( "SERVER = " + c.getData().name);
			         System.out.println();
			         if (chatRoomID <=rs.getInt("IDUSER")) {
			        	 chatRoomID=rs.getInt("IDUSER")+1;
			         }
			      }
			      rs.close();
			      stmt.close();
			      
			      
			      
					stmt = c.createStatement();	
				      rs = stmt.executeQuery( "SELECT * FROM MESSAGES;" );
				      while ( rs.next() ) {
				    	 ChatRoom c = getChatRoomFromID(rs.getInt("IDROOM"));
				    	 User us = getUserFromID(rs.getString("IDUSER"));
				    	 Calendar cal;
						c.postMessage(us, rs.getString("CONTENT"),rs.getString("TIME"));

				         System.out.println( "USER = " + rs.getString("IDUSER"));
				         System.out.println( "SERVER = " + c.getData().name);
				         System.out.println( "Message = " + rs.getString("CONTENT"));
				         System.out.println( "Date = " + rs.getString("TIME"));
				         System.out.println();
				      }  
				      rs.close();
				      stmt.close();
				      
				      
				      System.out.println("-----------");
				      stmt = c.createStatement();	
				      rs = stmt.executeQuery( "SELECT * FROM ASKJOIN;" );
				      while ( rs.next() ) {
				    	 ChatRoom c = getChatRoomFromID(rs.getInt("IDROOM"));
				    	 User us = getUserFromID(rs.getString("IDUSER"));
				    	 c.addAskingUser(us);
				         System.out.println( "USER = " + rs.getString("IDUSER"));
				         System.out.println( "SERVER = " + c.getData().name);
				         System.out.println();
				      }  
				      rs.close();
				      stmt.close();
	      } catch (SQLException e) {
				
				e.printStackTrace();
			}	      
	}
	//fonctions que le client peut appeller
	//permet de se créé un compte
	public boolean Register(String ID,String password) {
		//savoir si l'ID est déjà pris
		for(User p : users) {
			if(p.getUserData().getID().equals(ID))return false;
		}
		//on créé la personne et on l'enregistre
		User tmp= new User(ID,password);
		users.add(tmp);
		try {
			stmt = c.createStatement();
			
        String sql = "INSERT INTO USERS (LOGIN,NAME,PASSWORD,TOKEN) VALUES('"+tmp.getUserData().getID()+"','"+tmp.getUserData().getName()+"',"+tmp.getHashpaswword()+","+tmp.getToken()+")";
        stmt.executeUpdate(sql);
        stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
	}
	
	//permet de recuperer son token
	public Integer Login(String ID, String password, Client client) {
		User user = null;
		for(User p : users) {
			if(p.getUserData().getID().equals(ID)) {
				user = p;
				break;
			}
		}
		//savoir si l'ID existe
		if(user == null)return -2;//code erreur : ID inexistant
		//verification du mdp
		if(user.identify(password,client)) {
			return user.getToken();
		} else {
			return -1;//code erreur : mauvais mot de passe
		}
	}

	@Override
	public List<ChatRoomData> getChatRooms(int token) {
		List<ChatRoomData> rooms = new ArrayList<>();
		for(ChatRoom c : chatRooms) {
			if(c.isMember(token))rooms.add(c.getData());
		}
		return rooms;
	}
	
	@Override
	public boolean createChatRoom(int token, String name) {
		if(getUserFromToken(token) != null) {
			User user = getUserFromToken(token);
			int tempId =chatRoomID;
			chatRooms.add(new ChatRoom(user,chatRoomID,name));
			try {
				stmt = c.createStatement();
	        String sql = "INSERT INTO CHATROOM (ID,NAME,OWNER) VALUES('"+tempId+"','"+name+"','"+user.getUserData().getID()+"')";
	        stmt.executeUpdate(sql);
	        stmt.close();
			stmt = this.c.createStatement();
	        sql = "INSERT INTO MEMBERS (IDUSER,IDROOM) VALUES('"+user.getUserData().getID()+"','"+tempId+"')";
	        stmt.executeUpdate(sql);
	        stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			chatRoomID++;
			return true;
		}
		return false;
	}
	
	@Override
	public boolean addUserToChatRoom(int token, String ID, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return false;
		if (c.addUser(token,getUserFromID(ID))) {
		try {
		stmt = this.c.createStatement();
        String sql = "INSERT INTO MEMBERS (IDUSER,IDROOM) VALUES('"+ID+"','"+IDChatRoom+"')";
        stmt.executeUpdate(sql);
        stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		}
		return false;
	}

	@Override
	public List<Message> getMessagesFromChatRoom(int token, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c != null) {
			return c.getMessages(token);
		}
		return null;
	}

	@Override
	public boolean sendMessagesToChatRoom(int token, int IDChatRoom, String message) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		User u = getUserFromToken(token);
		if(c != null && u != null) {
			if (c.postMessage(u, message)) {
			try {
				stmt = this.c.createStatement();
		        String sql = "INSERT INTO MESSAGES (TIME,IDROOM,IDUSER,CONTENT) VALUES(datetime('now'),"+IDChatRoom+",'"+u.getUserData().getID()+"','"+message+"')";
		        stmt.executeUpdate(sql);
		        stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean changeName(int token, String newName) {
		User u = getUserFromToken(token);
		if(u != null) {
			 u.getUserData().setName(newName);
			 try {
				u.notifyClient(0);
			} catch (RemoteException e) {}
				try {
					stmt = c.createStatement();
					
		        String sql = "UPDATE USERS SET  NAME ='"+newName+"' WHERE '"+u.getUserData().getID()+"'=LOGIN";
		        stmt.executeUpdate(sql);
		        stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			 return true;
		}
		return false;
	}
	
	@Override
	public boolean banFromChatRoom(int token, String ID, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return false;
		if (c.banUser(token,getUserFromID(ID))) {
		try {
		stmt = this.c.createStatement();
        String sql = "DELETE FROM MEMBERS WHERE '"+ID+"'=IDUSER and IDROOM= "+IDChatRoom;
        stmt.executeUpdate(sql);
        stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return true;
		}
		return false;
	}

	@Override
	public boolean promoteUser(int token, String ID, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return false;
		if (c.promoteUser(token,getUserFromID(ID))) {
			try {
				stmt = this.c.createStatement();		
				String sql = "UPDATE CHATROOM SET  OWNER ='"+ID+"' WHERE '"+IDChatRoom+"'=ID";
				stmt.executeUpdate(sql);
				stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			return true;
		}
		return false;
	}	
	
	@Override
	public boolean isOwner(int token, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return false;
		return c.isOwner(token);
	}
	
	@Override
	public List<UserData> getUsersfromChatRoom(int token, int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return null;
		return c.getMembers(token);
	}
	
	@Override
	public boolean destroyChatRoom(int token, int IDChatRoom) throws RemoteException {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(c == null)return false;
		if(!c.isOwner(token))return false;
		//chatRooms.remove(c);
		int id = c.getData().getID();
		List<User> u = c.getMembersServer();
		c.destroy();
		try {
			stmt = this.c.createStatement();
	        String sql = "DELETE FROM MEMBERS WHERE IDROOM= "+IDChatRoom;
	        stmt.executeUpdate(sql);
	        stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		try {
			stmt = this.c.createStatement();
	        String sql = "DELETE FROM MESSAGES WHERE IDROOM= "+IDChatRoom;
	        stmt.executeUpdate(sql);
	        stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		try {
			stmt = this.c.createStatement();
	        String sql = "DELETE FROM CHATROOM WHERE ID= "+IDChatRoom;
	        stmt.executeUpdate(sql);
	        stmt.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		for(User p : u) {
			if(p.getToken() != token)p.notifyClient(id);
		}
		return true;
	}
	@Override
	public boolean askToJoin(int token,int IDChatRoom) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		User p = getUserFromToken(token);
		if (c == null || p == null) return false;
		if (c.addAskingUser(p)) {
			
			try {
				stmt = this.c.createStatement();
				String sql = "INSERT INTO ASKJOIN (IDUSER,IDROOM) VALUES('"+p.getUserData().getID()+"','"+IDChatRoom+"')";
		        stmt.executeUpdate(sql);
		        stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			return true;
		}
		return false;
	}
	
	
	@Override
	public boolean acceptJoin(int token , int IDChatRoom,String ID) {
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		User p = getUserFromID(ID);
		if (p == null  || c ==null) return false;
		if (c.confirmAskingUser(token, p)) {
			try {
				stmt = this.c.createStatement();
		        String sql = "DELETE FROM ASKJOIN WHERE IDROOM="+IDChatRoom+" and IDUSER='"+ID+"'";
		        stmt.executeUpdate(sql);
		        stmt.close();
		        stmt = this.c.createStatement();
		        sql = "INSERT INTO MEMBERS (IDUSER,IDROOM) VALUES('"+ID+"','"+IDChatRoom+"')";
		        stmt.executeUpdate(sql);
		        stmt.close();
				} catch (SQLException e) {
					e.printStackTrace();
			}
		return true;
		}
		return false;
	}
	@Override
	public List<ChatRoomDataExtended> getAllChatRooms(int token,String name) {
		User u = getUserFromToken(token);
		if(u == null) {
			return null;
		}
		List<ChatRoomDataExtended> chatRoomData= new ArrayList<>();
		for (ChatRoom r : chatRooms) {
			if ( name.contentEquals("") || (r.getData().name.length()>= name.length() &&  name.equals(r.getData().name.substring(0,name.length())))){
				ChatRoomStatus status = ChatRoomStatus.NOT_JOINED;
				if(r.isMember(token))status = ChatRoomStatus.JOINED;
				else if(r.isAskingUser(getUserFromToken(token)))status = ChatRoomStatus.WAITING_FOR_ACCEPTATION;
				chatRoomData.add(new ChatRoomDataExtended(r.getData(),status));
			}
		}
		return chatRoomData;
	}
	
	@Override
	public List<UserData> getAskingToJoin(int token, int IDChatRoom){
		User u = getUserFromToken(token);
		ChatRoom c = getChatRoomFromID(IDChatRoom);
		if(u == null || c == null) {
			return null;
		}
		return c.getWaitForAdding(u);
	}
	
	@Override
	public UserData getDataFromToken(int token) throws RemoteException {
		User u = getUserFromToken(token);
		if(u != null) {
			return u.getUserData();
		}
		return null;
	}
	
	//fonctions utilitaires
	private User getUserFromToken(int token) {
		for(User p : users) {
			if(p.getToken() == token) {
				return p;
			}
		}
		return null;
	}
	
	private User getUserFromID(String ID) {
		for(User p : users) {
			if(p.getUserData().getID().equals(ID)) {
				return p;
			}
		}
		return null;
	}

	private ChatRoom getChatRoomFromID(int IDChatRoom) {
		for(ChatRoom c : chatRooms) {
			if(c.getData().getID() == IDChatRoom) {
				return c;
			}
		}
		return null;
	}

	

	

	
}
