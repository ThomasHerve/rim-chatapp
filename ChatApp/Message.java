package ChatApp;

import java.util.Date;

public class Message implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private String content;
	private UserData author;
	private Date date;
	public Message(String content, UserData author) {
		this.content = content;
		this.author = author;
		this.date = new Date();
	}
	public Message(String content, UserData author,Date d) {
		this.content = content;
		this.author = author;
		this.date = d;
	}
	
	
	public String getContent() {
		return content;
	}
	
 
	public UserData getAuthor() {
		return author;
	}
	
	public Date getDate() {
		return date;
	}
	
}
