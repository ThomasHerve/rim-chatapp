package ChatApp;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface Server extends Remote{
	/*
	 *NOTES:
	 * Chaque utilisateur est identifié par un mot de passe et un ID. Par simplicité et éviter de faire transité ces infos à chaque appelle distant,
	 * On utilise un "token" unique, associé à l'utilisateur.
	 * */
	public boolean Register(String ID,String password) throws RemoteException;
	public Integer Login(String ID, String password, Client client) throws RemoteException;//-2 = nom inconnu, -1 = mauvais mot de passe
	public boolean createChatRoom(int token, String name) throws RemoteException;//permet de créé une ChatRoom
	public List<ChatRoomData> getChatRooms(int token) throws RemoteException;//permet d'obtenir toute les chatRoom qu'un utilisateur à rejoin
	public boolean addUserToChatRoom(int token, String ID, int IDChatRoom) throws RemoteException;//permet d'ajouter un utilisateur à une chatRoom
	public List<Message> getMessagesFromChatRoom(int token, int IDChatRoom) throws RemoteException;//permet d'obtenir la liste des messages publié sur une chatRoom
	public List<UserData> getUsersfromChatRoom(int token, int IDChatRoom) throws RemoteException;//permet d'obtenir la liste de tout les utilisateurs présent sur la room
	public UserData getDataFromToken(int token) throws RemoteException;//Permet d'obtenir ses données (ID, nom...) à partir de son token
	public boolean sendMessagesToChatRoom(int token, int IDChatRoom, String message) throws RemoteException;//Permet d'envoyer un message à une ChatRoom
	public boolean changeName(int token, String newName) throws RemoteException;//Permet de changer de nom (le pseudonyme, à ne pas confondre avec l'ID (que l'on peut aussi appeller "login")
	public boolean banFromChatRoom(int token, String ID, int IDChatRoom) throws RemoteException;//permet de bannir un utilisateur d'une chatRoom, seulement si le token appartient au possesseur (owner) de la ChatRoom
	public boolean promoteUser(int token, String ID, int IDChatRoom) throws RemoteException;//permet de transmettre la possession de la ChatRoom à un autre User
	public boolean isOwner(int token, int IDChatRoom) throws RemoteException;//permet de savoir si l'on est l'owner de la ChatRoom
	public boolean destroyChatRoom(int token, int IDChatRoom) throws RemoteException;//permet de détruire la ChatRoom
	public boolean askToJoin(int token,int IDChatRoom) throws RemoteException;//permet de demander de rejoindre une ChatRoom
	public boolean acceptJoin(int token , int IDChatRoom,String ID)throws RemoteException;//permet d'accepter la demande d'un utilisateur de rejoindre la ChatRoom
	public List<ChatRoomDataExtended> getAllChatRooms(int token,String name)throws RemoteException;//permet d'obtenir toutes les ChatsRoom dont le nom commence par "name" + la relation que l'utilisateur a avec cette ChatRoom
	public List<UserData> getAskingToJoin(int token, int IDChatRoom)throws RemoteException;//permet a l'owner d'obtenir toute les demandes de rejoinde la ChatRoom
}
