package ChatApp;


//classe sécurisé pour permettre au client d'avoir des informations sur une personne, sans les données sensibles
public class UserData implements java.io.Serializable{
	private static final long serialVersionUID = 1L;
	private String ID;//ID de l'utilisateur, unique
	private String name;//nom de l'utilisateur, pas unique
	
	public UserData(String ID, String name){
		this.ID = ID;
		this.name = name;
	}	
	
	public String getID() {
		return ID;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
}
