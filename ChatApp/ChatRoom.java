package ChatApp;

import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

public class ChatRoom {
	private User owner;//Possesseur de la ChatRoom
	private ChatRoomData data;//Les données de la ChatRoom accessible à distance
	private List<User> users;//membres de la room
	private List<Message> messages;//la liste des messages publiés sur la ChatRoom
	private List<User> waitforadding;//les users qui veulent rejoindre la ChatRoom
	
	//synchronisation
	private ReentrantLock lockUsers;
	private ReentrantLock lockMessages;
	
	public ChatRoom(User creator,int ID, String name) {
		users = new ArrayList<>();
		waitforadding = new ArrayList<>();
		users.add(creator);
		messages = new ArrayList<>();
		data = new ChatRoomData(ID,name,creator.getUserData());
		lockUsers = new ReentrantLock();
		lockMessages = new ReentrantLock();
		owner = creator;
		
	}
	
	//Getters
	public User getOwner() {
		return owner;
	}
	
	public ChatRoomData getData() {
		return data;
	}
	
	public List<Message> getMessages(int token){
		if(!isMember(token))return null;
		return messages;
	}
	
	public List<UserData> getMembers(int token){
		if(!isMember(token))return null;
		List<UserData> list = new ArrayList<>();
		for(User p : users) {
			list.add(p.getUserData());
		}
		return list;
	}
	
	public List<User> getMembersServer(){
		return users;
	}
		
	//gestion des utilisateurs
	public boolean addUser(int token,User p) {
		if(p == null)return false;
		lockUsers.lock();
		if(users.contains(p) || !isMember(token)) {
			lockUsers.unlock();
			return false;
		}
		users.add(p);
		lockUsers.unlock();
		try {
			owner.notifyClient(data.getID());
			p.notifyClient(data.getID());
		} catch (RemoteException e) {}
		return true;
	}
	
	public boolean addUser(User p) {
		if(p == null)return false;
		lockUsers.lock();
		if(users.contains(p)) {
			lockUsers.unlock();
			return false;
		}
		users.add(p);
		lockUsers.unlock();
		try {
			owner.notifyClient(data.getID());
			p.notifyClient(data.getID());
		} catch (RemoteException e) {}
		return true;
	}
	
	public boolean addAskingUser(User p) {
		if (p == null) return false;
		lockUsers.lock();
		if(waitforadding.contains(p) || users.contains(p)) {
			lockUsers.unlock();
			return false;
		}
		waitforadding.add(p);
		lockUsers.unlock();
		return true;
	}
	
	public boolean isAskingUser(User p) {
		return waitforadding.contains(p);
	}
	
	public boolean confirmAskingUser(int token ,User p) {
		if(p == null)return false;
		lockUsers.lock();
		if(!waitforadding.contains(p) || p == owner || users.contains(p)) {
			lockUsers.unlock();
			return false;
		}
		if(token != owner.getToken() && token != p.getToken()){
			lockUsers.unlock();
			return false;
		}
		waitforadding.remove(p);
		users.add(p);
		lockUsers.unlock();
		try {
			owner.notifyClient(data.getID());
			p.notifyClient(data.getID());
		} catch (RemoteException e) {}
		return true;
	}
	
	public boolean banUser(int token,User p) {
		if(p == null)return false;
		lockUsers.lock();
		//tu ne peut pas bannir si : l'utilisateur n'appartient pas à la room ou si tu n'es pas l'owner et que tu cherche a bannir quelqun qui n'est pas toi même, ou si l'owner cherche à se bannir lui même
		if(!users.contains(p) || p == owner) {
			lockUsers.unlock();
			return false;
		}
		if(token != owner.getToken() && token != p.getToken()){
			lockUsers.unlock();
			return false;
		}
		users.remove(p);
		lockUsers.unlock();
		try {
			owner.notifyClient(data.getID());
			p.notifyClient(data.getID());
		} catch (RemoteException e) {}
		return true;
	}
	
	public void destroy() {
		lockUsers.lock();
		users = new ArrayList<>();
		lockUsers.unlock();
	}
	
	public boolean promoteUser(int token, User p) {
		if(p == null)return false;
		lockUsers.lock();
		if(!users.contains(p) || token != owner.getToken()) {
			lockUsers.unlock();
			return false;
		}
		User oldowner = owner;
		data.setOwner(p.getUserData());
		owner = p;
		lockUsers.unlock();
		try {
			p.notifyClient(data.getID());
			oldowner.notifyClient(data.getID());
		} catch (RemoteException e) {}
		return true;
	}
	
	//envoie de messages
	public boolean postMessage(User user,String message) {
		lockMessages.lock();
		if(!isMember(user.getToken())) {
			lockMessages.unlock();
			return false;
		}
		messages.add(new Message(message,user.getUserData()));
		lockMessages.unlock();
		notifyAllMembers();
		return true;
	}
	public boolean postMessage(User user,String message,String d) {
		lockMessages.lock();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date= new Date();
		try {
			date = formatter.parse(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		messages.add(new Message(message,user.getUserData(),date));
		lockMessages.unlock();
		notifyAllMembers();
		return true;
	}
	
	public boolean isOwner(int token) {
		//TODO : BUG tout le temps true
		return token == owner.getToken();
	}
	
	public List<UserData> getUsers(int token){
		if(!isMember(token)) {
			return null;
		}
		List<UserData> usersData = new ArrayList<>();
		for(User p : users) {
			usersData.add(p.getUserData());
		}
		return usersData;
	}
	
	public List<UserData> getWaitForAdding(User u){
		if(u != owner)return null;
		List<UserData> data = new ArrayList<>();
		for(User user : waitforadding) {
			data.add(user.getUserData());
		}
		return data;
	}
	
	//fonctions utilitaires
	
	//savoir si le token appartient à un membre de la room, sert ici de sécurité
	public boolean isMember(int token) {
		for(User p : users) {
			if(p.getToken() == token)return true;
		}
		return false;
	}
	
	//on veux notifier tout les membres que la room a été mis à jour
	//il se peux que l'objet distant soit inaccessible, par simplicité on fait un try {}
	private void notifyAllMembers() {
		for(User p : users) {
			try {
				p.notifyClient(data.getID());
			} catch(Exception e) {}
		}
	}
	
	
}
