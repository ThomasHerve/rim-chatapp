package ChatApp;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Client extends Remote {
	public void refresh(int ID) throws RemoteException;
}
