package ChatApp;

import java.util.ArrayList;
import java.util.List;

public class ClientImpl implements Client{
	private List<ClientRefreshHandler> handlers;
	
	public ClientImpl() {
		handlers = new ArrayList<>();
	}
	
	@Override
	public void refresh(int ID) {
		for(ClientRefreshHandler c : handlers) {
			c.run(ID);
		}
	}
	
	public void register(ClientRefreshHandler ref) {
		handlers.add(ref);
	}
	
}
