package ChatApp;

import java.rmi.RemoteException;

public class User {
	public static int TOKEN_COUNTER = 0;
	private UserData userData;
	private int hashPassword;//on ne veut pas stocker de mot de passe sur notre serveur
	private int token;//permet d'identifier une personne, supposément unique. Ici on ne s'embete pas on reprend le hash du mdp, mais cela ne garantit pas l'unicité du token
	private Client client;//objet distant permettant aux chartRoom de notifier les clients d'une update
	
	public User(String ID, String password){
		userData = new UserData(ID,ID);
		this.hashPassword = password.hashCode();
		this.token = TOKEN_COUNTER++;
	}
	public User(String ID,String name,int hashPassword,int token) {
		userData = new UserData(ID,name);
		this.hashPassword =hashPassword;
		this.token = token;
		if (token >=TOKEN_COUNTER) {
				TOKEN_COUNTER=token+1;
		}
	}
	
	public int getHashpaswword() {
		return hashPassword;
	}

	public int getHashCode() {
		return hashPassword;
	}
	
	public UserData getUserData() {
		return userData;
	}
	
	public Boolean identify(String password, Client client) {
		if(password.hashCode() == hashPassword) {
			this.client = client;
			return true;
		} else {
			return false;
		}
	}
	
	public Integer getToken() {
		return token;
	}
	
	public Boolean compareToken(Integer t) {
		return t == token;
	}
	
	public void notifyClient(int roomID) throws RemoteException {
		if(client != null) {
			client.refresh(roomID);
		}
	}
	
}
