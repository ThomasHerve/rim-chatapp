package Graphic;

import java.rmi.RemoteException;

import javax.swing.text.AbstractDocument.Content;

import ChatApp.ChatRoomData;
import ChatApp.ClientRefreshHandler;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ChatRoomSelectionScene implements ChatScene{
	ClientGraphique clientGraphique;
	Stage primaryStage;
	Scene scene;
	Label error;
	Label nameLabel;
	
	//tableview elements
    private final TableView<Room> table = new TableView<>();
    private ObservableList<Room> rooms = FXCollections.observableArrayList();
	
	public ChatRoomSelectionScene(ClientGraphique clientGraphique, Stage primaryStage) {
		this.clientGraphique = clientGraphique;
		this.primaryStage = primaryStage;
		BorderPane root = new BorderPane();
		root.getStylesheets().add("Style.css");
		scene = new Scene(root,400,400);
		
		//title
		Label title = new Label("Room selection");
		title.setFont(new Font("Verdana",20));
		HBox hboxTop = new HBox();
		Button change = new Button("Change name");
		TextField name = new TextField();
		Label chnageNameLabel = new Label("Change name :");
		hboxTop.getChildren().addAll(chnageNameLabel,name,change);
		hboxTop.setAlignment(Pos.CENTER);
		nameLabel = new Label();
		nameLabel.setFont(new Font("Verdana",14));
		VBox top = new VBox();
		top.getChildren().addAll(title,hboxTop,nameLabel);
		top.setAlignment(Pos.CENTER);
		root.setTop(top);
		BorderPane.setAlignment(title, Pos.CENTER);
		
		//room création
		Button create = new Button("Create");
		TextField roomName = new TextField();
		Label createARoom = new Label("Create a room :");
		error = new Label();
		error.setTextFill(new Color(1.0,0.0,0.0,1));
		createARoom.setFont(new Font("Verdana",14));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(createARoom,roomName,create,error);
		VBox vboxBottom = new VBox();
		Button buttonChooseRoom = new Button("Go to Choose ChatRoom");
		vboxBottom.getChildren().addAll(hbox,buttonChooseRoom);
		root.setBottom(vboxBottom);
		hbox.setAlignment(Pos.CENTER);
		vboxBottom.setAlignment(Pos.CENTER);
		
		create.setOnAction((e)->{
			if(roomName.getText().length() == 0) {
				error.setText("Please enter a name");
			} else {
				try {
					clientGraphique.getServer().createChatRoom(ClientGraphique.TOKEN, roomName.getText());
				} catch (RemoteException e1) {
					clientGraphique.errorHandler();
				}
				error.setText("");
				update();
			}
		});
		
		change.setOnAction((e)->{
			try {
				clientGraphique.getServer().changeName(ClientGraphique.TOKEN, name.getText());
			} catch (RemoteException e1) {
				clientGraphique.errorHandler();
			}
		});
		
		buttonChooseRoom.setOnAction((e)->{
			clientGraphique.loadScene(4);
		});
		
		//the rooms
		TableColumn names = new TableColumn("Name");
		names.setMinWidth(100);
		names.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn buttons = new TableColumn("");
		buttons.setMinWidth(100);
		buttons.setCellValueFactory(new PropertyValueFactory<>("button"));
		table.setItems(rooms);
        table.getColumns().addAll(names, buttons);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        root.setCenter(table);
		
		
		//register update
		(clientGraphique.getClient()).register(new ClientRefreshHandler() {
			public void run(int ID) {
				Platform.runLater(()->{
					update();
				});
			}
		});
		
	}
	@Override
	public void loadScene() {
		update();
		primaryStage.setScene(scene);
	}
	
	public static class Room {
		private String name;
		private Button button;
		private HBox hbox;
		
		private Room(String name, int id,ClientGraphique clientGraphique) {
			this.name = name;
			button = new Button("Join");
			button.setOnAction((e)->{
				ChatRoomScene.CURRENT_CHAT_ROOM = id;
				clientGraphique.loadScene(3);
			});
			hbox = new HBox();
			hbox.setAlignment(Pos.CENTER);
			hbox.getChildren().add(button);
		}
		
		public String getName() {
			return name;
		}
		
		public HBox getButton() {
			return hbox;
		}
	}
	
	private void update() {
		rooms = FXCollections.observableArrayList();
		try {
			for(ChatRoomData rd : clientGraphique.getServer().getChatRooms(ClientGraphique.TOKEN)) {
				rooms.add(new Room(rd.getName(),rd.getID(), clientGraphique));
			}
			table.setItems(rooms);
			nameLabel.setText("Current name : "+clientGraphique.getServer().getDataFromToken(ClientGraphique.TOKEN).getName());
		} catch (RemoteException e) {
			clientGraphique.errorHandler();
		}
	}
	
	


}
