package Graphic;

import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.locks.ReentrantLock;

import javax.swing.text.AbstractDocument.Content;

import ChatApp.ChatRoomData;
import ChatApp.ClientRefreshHandler;
import ChatApp.Message;
import ChatApp.UserData;
import Graphic.ChatRoomSelectionScene.Room;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.util.Callback;

public class ChatRoomScene implements ChatScene{
	public static int CURRENT_CHAT_ROOM;
	ClientGraphique clientGraphique;
	Stage primaryStage;
	Scene scene;
	Label error;
	Menu menuBan;
	Menu menuPromote;
	Menu menuAccept;
	MenuBar menuBar;
	Menu option;
	BorderPane root;
	MenuItem menuQuit;
	
	List<Message> messages;
	private ObservableList<MessageObservable> messagesObservable = FXCollections.observableArrayList();
    private final TableView<MessageObservable> table = new TableView<>();

	//synchro
	ReentrantLock lock = new ReentrantLock();
	

	public ChatRoomScene(ClientGraphique clientGraphique, Stage primaryStage) {
		this.clientGraphique = clientGraphique;
		this.primaryStage = primaryStage;
		root = new BorderPane();
		root.getStylesheets().add("Style.css");
		scene = new Scene(root,400,400);
		
		//poster un message
		Button post = new Button("Post");
		TextField text = new TextField();
		Label yourmessage = new Label("Your message :");
		yourmessage.setFont(new Font("Verdana",14));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(yourmessage,text,post);
		HBox.setMargin(post, new Insets(0,40,10,40));
		hbox.setAlignment(Pos.CENTER);
		
		//inviter quelqun
		Button add = new Button("Add");
		TextField name = new TextField();
		Label user = new Label("Add someone :");
		yourmessage.setFont(new Font("Verdana",14));
		HBox hbox2 = new HBox();
		hbox2.getChildren().addAll(user,name,add);
		HBox.setMargin(add, new Insets(0,40,0,40));
		hbox2.setAlignment(Pos.CENTER);
		
		error = new Label();

		VBox vbox = new VBox();
		vbox.getChildren().addAll(hbox,hbox2,error);
		vbox.setAlignment(Pos.CENTER);
		root.setBottom(vbox);
		
		//messages 
		
		TableColumn names = new TableColumn("Name");
		names.setMinWidth(100);
		names.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn buttons = new TableColumn("Message");
		buttons.setMinWidth(100);
		buttons.setCellValueFactory(new PropertyValueFactory<>("content"));
		buttons.setCellFactory(WRAPPING_CELL_FACTORY);
		table.setItems(messagesObservable);
        table.getColumns().addAll(names, buttons);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        root.setCenter(table);
        
		//options
	    menuBar = new MenuBar();
	    menuBar.prefWidthProperty().bind(primaryStage.widthProperty());
	    
	    option = new Menu("Options");
	    menuQuit = new MenuItem("Quit");
	    MenuItem menuBackToServerList = new MenuItem("Back to Chat rooms list");
	    option.getItems().addAll(menuQuit,new SeparatorMenuItem(),menuBackToServerList);
	    menuBan = new Menu("Ban");
	    menuPromote = new Menu("Promote");
	    menuAccept = new Menu("Accept");
	    menuBar.getMenus().addAll(option,menuBan,menuPromote,menuAccept);
		root.setTop(menuBar);
		
		//events
		post.setOnAction((e)->{
			if(text.getText().length()>0) {
				try {
					clientGraphique.getServer().sendMessagesToChatRoom(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM, text.getText());
				} catch (RemoteException e1) {clientGraphique.errorHandler();}
			}
		});
		
		add.setOnAction((e)->{
			try {
				if(!clientGraphique.getServer().addUserToChatRoom(ClientGraphique.TOKEN, name.getText(), CURRENT_CHAT_ROOM)) {
					error.setText("Unknown user");
					error.setTextFill(new Color(1.0,0.0,0.0,1));
				} else {
					error.setText("User added");
					error.setTextFill(new Color(0.0,1.0,0.0,1));
				}
			} catch (RemoteException e1) {clientGraphique.errorHandler();}
		});
		
		menuQuit.setOnAction((e)->{
			try {
				if(clientGraphique.getServer().isOwner(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM)) {
					//Detruire la chatRoom
					lock.lock();
					clientGraphique.getServer().destroyChatRoom(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM);
					lock.unlock();
					clientGraphique.loadScene(2);
				} else {
					//Quitter la chat room
					clientGraphique.getServer().banFromChatRoom(ClientGraphique.TOKEN, ClientGraphique.name, CURRENT_CHAT_ROOM);
					update();
				}
			} catch (RemoteException e1) {clientGraphique.errorHandler();} 
		});
		
		menuBackToServerList.setOnAction((e)->{
			clientGraphique.loadScene(2);
		});

		//register
		(clientGraphique.getClient()).register(new ClientRefreshHandler() {
			public void run(int ID) {
				update();
			}
		});
	}
	
	@Override
	public void loadScene() {
		update();
		primaryStage.setScene(scene);
		Platform.runLater(()->{
			ScrollBar verticalBar = (ScrollBar) table.lookup(".scroll-bar:vertical");
			if(verticalBar != null)verticalBar.setValue(1.0);
		});
	}
	
	private void update() {
		lock.lock();
		try {
			//check if not banned
			boolean member = false;
			for(ChatRoomData rd : clientGraphique.getServer().getChatRooms(ClientGraphique.TOKEN)) {
				if(rd.getID() == CURRENT_CHAT_ROOM)member = true;
			}
			if(!member) {
				Platform.runLater(()->{
					clientGraphique.loadScene(2);
				});
			}
			
			//check if owner
			menuBar = new MenuBar();
			Platform.runLater(()->{
				root.setTop(menuBar);
			});

			if(clientGraphique.getServer().isOwner(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM)) {
				menuQuit.setText("Destroy chatRoom");
				menuBan = new Menu("Ban");
				menuPromote = new Menu("Promote");
			    menuAccept = new Menu("Accept");
				for(UserData u : clientGraphique.getServer().getUsersfromChatRoom(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM)) {
					if(!u.getID().equals(ClientGraphique.name)) {//on ne peut pas bannir ou promouvoir l'owner
						MenuItem userBan = new MenuItem(u.getName() + "("+u.getName()+")");
						MenuItem userPromote = new MenuItem(u.getName() + "("+u.getName()+")");
						menuBan.getItems().add(userBan);
						menuPromote.getItems().add(userPromote);
						userBan.setOnAction((e)->{
							try {
								clientGraphique.getServer().banFromChatRoom(ClientGraphique.TOKEN, u.getID(), CURRENT_CHAT_ROOM);
								error.setText(u.getName() + " banned !");
								error.setTextFill(new Color(0.0,1.0,0.0,1));
							} catch (RemoteException e1) {clientGraphique.errorHandler();}
						});
						userPromote.setOnAction((e)->{
							try {
								clientGraphique.getServer().promoteUser(ClientGraphique.TOKEN, u.getID(), CURRENT_CHAT_ROOM);
								error.setText(u.getName() + " promoted !");
								error.setTextFill(new Color(0.0,1.0,0.0,1));
							} catch (RemoteException e1) {clientGraphique.errorHandler();}
						});
					}
				}
				for(UserData u : clientGraphique.getServer().getAskingToJoin(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM)) {
					MenuItem userAccept = new MenuItem(u.getName() + "("+u.getName()+")");
					menuAccept.getItems().add(userAccept);
					userAccept.setOnAction((e)->{
						try {
							clientGraphique.getServer().acceptJoin(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM,u.getID());
							error.setText(u.getName() + " accepted !");
							error.setTextFill(new Color(0.0,1.0,0.0,1));
						} catch (RemoteException e1) {clientGraphique.errorHandler();}
					});
				}
			    menuBar.getMenus().addAll(option,menuBan,menuPromote,menuAccept);
			} else {
				menuQuit.setText("Quit");
				menuBar.getMenus().addAll(option);
			}
		} catch (RemoteException e) {clientGraphique.errorHandler();}
		//mettre à jour les messages
		try {
			messages = clientGraphique.getServer().getMessagesFromChatRoom(ClientGraphique.TOKEN, CURRENT_CHAT_ROOM);
		} catch (RemoteException e) {clientGraphique.errorHandler();}
		Platform.runLater(()->{
			//mettre à jour graphiquement
			messagesObservable = FXCollections.observableArrayList();
			if (messages != null  && !messages.isEmpty())
			for(Message m : messages) {
				messagesObservable.add(new MessageObservable(m));
			}
			table.setItems(messagesObservable);
			ScrollBar verticalBar = (ScrollBar) table.lookup(".scroll-bar:vertical");
			if(verticalBar != null)verticalBar.setValue(1.0);
		});
		lock.unlock();
	}
	
	public static class MessageObservable {
		private String name;
		private String ID;
		private Date date;
		private String content;
		private MessageObservable(Message m) {
			name = m.getAuthor().getName();
			ID = m.getAuthor().getID();
			date = m.getDate();
			content = m.getContent();
		}
		
		public String getName() {
			return name;
		}
		
		public String getContent() {
			return content;
		}
	}
	
	public static final Callback<TableColumn<Content,String>, TableCell<Content,String>> WRAPPING_CELL_FACTORY = 
            new Callback<TableColumn<Content,String>, TableCell<Content,String>>() {
                
        @Override public TableCell<Content,String> call(TableColumn<Content,String> param) {
            TableCell<Content,String> tableCell = new TableCell<Content,String>() {
                @Override protected void updateItem(String item, boolean empty) {
                    if (item == getItem()) return;

                    super.updateItem(item, empty);

                    if (item == null) {
                        super.setText(null);
                        super.setGraphic(null);
                    } else {
                        super.setText(null);
                        Label l = new Label(item);
                        l.setWrapText(true);
                        VBox box = new VBox(l);
                        l.heightProperty().addListener((observable,oldValue,newValue)-> {
                        	box.setPrefHeight(newValue.doubleValue()+7);
                        	Platform.runLater(()->this.getTableRow().requestLayout());
                        });
                        super.setGraphic(box);
                    }
                }
            };
	    return tableCell;
        }
    };
	
	
}
