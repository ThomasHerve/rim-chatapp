package Graphic;

import java.rmi.RemoteException;

import ChatApp.ChatRoomDataExtended;
import ChatApp.ChatRoomStatus;
import ChatApp.ClientRefreshHandler;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ChooseRoom implements ChatScene{
	ClientGraphique clientGraphique;
	Stage primaryStage;
	Scene scene;
	Label error;
	TextField roomName;
	
	//tableview elements
    private final TableView<Room> table = new TableView<>();
    private ObservableList<Room> rooms = FXCollections.observableArrayList();
	
	public ChooseRoom(ClientGraphique clientGraphique, Stage primaryStage) {
		this.clientGraphique = clientGraphique;
		this.primaryStage = primaryStage;
		BorderPane root = new BorderPane();
		root.getStylesheets().add("Style.css");
		scene = new Scene(root,400,400);
		
		//title
		Label title = new Label("Choose a Room to join");
		title.setFont(new Font("Verdana",20));
		root.setTop(title);
		BorderPane.setAlignment(title, Pos.CENTER);
		
		//room création
		Button refresh = new Button("Filter");
		roomName = new TextField();
		Label createARoom = new Label("Filter a room :");
		error = new Label();
		error.setTextFill(new Color(1.0,0.0,0.0,1));
		createARoom.setFont(new Font("Verdana",14));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(createARoom,roomName,refresh,error);
		VBox vboxBottom = new VBox();
		Button buttonChooseRoom = new Button("Go to ChatRooms list");
		vboxBottom.getChildren().addAll(hbox,buttonChooseRoom);
		root.setBottom(vboxBottom);
		hbox.setAlignment(Pos.CENTER);
		vboxBottom.setAlignment(Pos.CENTER);
		
		refresh.setOnAction((e)->{
			update();
		});
		
		buttonChooseRoom.setOnAction((e)->{
			clientGraphique.loadScene(2);
		});
		
		//the rooms
		TableColumn names = new TableColumn("Name");
		names.setMinWidth(100);
		names.setCellValueFactory(new PropertyValueFactory<>("name"));
		TableColumn buttons = new TableColumn("");
		buttons.setMinWidth(100);
		buttons.setCellValueFactory(new PropertyValueFactory<>("button"));
		table.setItems(rooms);
        table.getColumns().addAll(names, buttons);
        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        root.setCenter(table);
		
		
		//register update
		(clientGraphique.getClient()).register(new ClientRefreshHandler() {
			public void run(int ID) {
				Platform.runLater(()->{
					update();
				});
			}
		});
		
	}
	@Override
	public void loadScene() {
		update();
		primaryStage.setScene(scene);
	}
	
	public static class Room {
		private String name;
		private Button button;
		private HBox hbox;
		
		private Room(ChatRoomDataExtended rd, ClientGraphique clientGraphique, ChooseRoom currentScene) {
			this.name = rd.getName();
			button = new Button();
			switch(rd.getStatus()) {
				case JOINED:
					button.setText("Already joined");
					break;
				case NOT_JOINED:
					button.setText("Ask to join");
					button.setOnAction((e)->{
						try {
							clientGraphique.getServer().askToJoin(ClientGraphique.TOKEN, rd.getID());
							currentScene.update();
						} catch (RemoteException e1) {clientGraphique.errorHandler();}
					});
					break;
				default:
					button.setText("Waiting for acceptation");
					break;
			}
			
			hbox = new HBox();
			hbox.setAlignment(Pos.CENTER);
			hbox.getChildren().add(button);
		}
		
		public String getName() {
			return name;
		}
		
		public HBox getButton() {
			return hbox;
		}
	}
	
	private void update() {
		rooms = FXCollections.observableArrayList();
		try {
			for(ChatRoomDataExtended rd : clientGraphique.getServer().getAllChatRooms(ClientGraphique.TOKEN,roomName.getText())) {
				rooms.add(new Room(rd, clientGraphique, this));
			}
			table.setItems(rooms);
		} catch (RemoteException e) {
			clientGraphique.errorHandler();
		}
	}
}
