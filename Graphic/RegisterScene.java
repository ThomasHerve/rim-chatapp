package Graphic;

import ChatApp.Server;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class RegisterScene implements ChatScene{
	ClientGraphique clientGraphique;
	Stage primaryStage;
	Scene scene;
	
	Label error;
	public RegisterScene(ClientGraphique clientGraphique, Stage primaryStage) {
		this.clientGraphique = clientGraphique;
		this.primaryStage = primaryStage;
		BorderPane root = new BorderPane();
		root.getStylesheets().add("Style.css");
		scene = new Scene(root,400,400);
		
		//title
		Label title = new Label("register");
		title.setFont(new Font("Verdana",20));
		root.setTop(title);
		BorderPane.setAlignment(title, Pos.CENTER);
		
		//Textfields
		VBox box = new VBox();
		TextField username = new TextField("username");
		TextField password = new TextField("password");
		Button register = new Button("Register");
		error = new Label("");

		box.getChildren().addAll(username,password,register,error);
		box.setAlignment(Pos.CENTER);
		VBox.setMargin(username, new Insets(40,40,40,40));
		VBox.setMargin(password, new Insets(10,40,40,40));
		VBox.setMargin(register, new Insets(0,40,40,40));
		VBox.setMargin(error, new Insets(0,40,40,40));
		
		root.setCenter(box);
		BorderPane.setAlignment(box, Pos.CENTER);
		
		//go login
		Button login = new Button("Login");
		Label notregistered = new Label("Already have an account ? go on :");
		notregistered.setFont(new Font("Verdana",14));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(notregistered,login);
		root.setBottom(hbox);
		hbox.setAlignment(Pos.CENTER);
		//events
		register.setOnAction((e)->{
			try {
				if(clientGraphique.getServer().Register(username.getText(), password.getText())) {
					error.setTextFill(new Color(0.0,1.0,0.0,1));
					error.setText("account created, go to login !");
				} else {
					error.setTextFill(new Color(1.0,0.0,0.0,1));
					error.setText("Username already taken !");
				}
			} catch (Exception e1) {
				clientGraphique.errorHandler();
			}
		});
		
		login.setOnAction((e)->{
			clientGraphique.loadScene(0);
		});
	}

	@Override
	public void loadScene() {
		primaryStage.setScene(scene);
	}
}
