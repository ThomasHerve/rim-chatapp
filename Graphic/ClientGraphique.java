package Graphic;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import ChatApp.ChatRoomData;
import ChatApp.Client;
import ChatApp.ClientImpl;
import ChatApp.Server;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.stage.Stage;

public class ClientGraphique extends Application{
	public static int TOKEN;
	public static String name;
	Server server;
	ClientImpl client;
	
	ChatScene loginScene;
	ChatScene registerScene;
	ChatScene chatRoomSelectionScene;
	ChatScene chatRoom;
	ChatScene chooseRoom;
	
	public static void main(String[] args) {
        Application.launch(args);
    }
	
	public void start(Stage primaryStage) {
		primaryStage.setTitle("RIM ChatAPP");
		primaryStage.setOnCloseRequest(e -> {Platform.exit();System.exit(0);});

		Registry registry;
		try {
			registry = LocateRegistry.getRegistry();
			 client = new ClientImpl ();
			 Client h_stub = (Client) UnicastRemoteObject.exportObject(client, 0);
			 registry.rebind("CLient", h_stub);
			 // Get remote object reference
			 server = (Server) registry.lookup("Server");
			 
		} catch (Exception e) {errorHandler();}
		setup(primaryStage);
		loadScene(0);
        primaryStage.show();
	}
	
	
	public Server getServer() {
		return server;
	}
	
	public ClientImpl getClient() {
		return client;
	}
	
	public void errorHandler() {
		System.out.println("Server unreachable");
	}
	
	public void loadScene(int index) {
		switch(index) {
		case 0:
			loginScene.loadScene();
			return;
		case 1:
			registerScene.loadScene();
			return;
		case 2:
			chatRoomSelectionScene.loadScene();
			return;
		case 3:
			chatRoom.loadScene();
			return;
		case 4:
			chooseRoom.loadScene();
			return;
		}
	}
	
	//utilitaire
	private void setup(Stage primaryStage) {
		loginScene = new LoginScene(this,primaryStage);
		registerScene = new RegisterScene(this,primaryStage);
		chatRoomSelectionScene = new ChatRoomSelectionScene(this,primaryStage);
		chatRoom = new ChatRoomScene(this,primaryStage);
		chooseRoom = new ChooseRoom(this,primaryStage);
	}
	
}
