package Graphic;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class LoginScene implements ChatScene {
	ClientGraphique clientGraphique;
	Stage primaryStage;
	Scene scene;
	
	Label error;
	public LoginScene(ClientGraphique clientGraphique, Stage primaryStage) {
		this.clientGraphique = clientGraphique;
		this.primaryStage = primaryStage;
		BorderPane root = new BorderPane();
		root.getStylesheets().add("Style.css");
		scene = new Scene(root,400,400);
		//title
		Label title = new Label("Login");
		title.setFont(new Font("Verdana",20));
		root.setTop(title);
		BorderPane.setAlignment(title, Pos.CENTER);
		
		//Textfields
		VBox box = new VBox();
		TextField username = new TextField("username");
		TextField password = new TextField("password");
		Button login = new Button("login");
		error = new Label("");
		error.setTextFill(new Color(1.0,0.0,0.0,1));

		box.getChildren().addAll(username,password,login,error);
		box.setAlignment(Pos.CENTER);
		VBox.setMargin(username, new Insets(40,40,40,40));
		VBox.setMargin(password, new Insets(10,40,40,40));
		VBox.setMargin(login, new Insets(0,40,40,40));
		VBox.setMargin(error, new Insets(0,40,40,40));
		
		root.setCenter(box);
		BorderPane.setAlignment(box, Pos.CENTER);
		
		//go register
		Button register = new Button("Register");
		Label notregistered = new Label("Not registered ? go on :");
		notregistered.setFont(new Font("Verdana",14));
		HBox hbox = new HBox();
		hbox.getChildren().addAll(notregistered,register);
		root.setBottom(hbox);
		hbox.setAlignment(Pos.CENTER);
		//events
		login.setOnAction((e)->{
			try {
				int res;
				switch(res =  clientGraphique.getServer().Login(username.getText(), password.getText(),clientGraphique.getClient())) {
					case -2:
						error.setText("Unknown username");
						break;
					case -1:
						error.setText("Wrong password");
						break;
					default:
						ClientGraphique.TOKEN = res;
						ClientGraphique.name = username.getText();
						primaryStage.setTitle("RIM ChatAPP, connected as: "+username.getText());
						clientGraphique.loadScene(2);
						break;
				}
			} catch (Exception e1) {
				clientGraphique.errorHandler();
			}
		});
		
		register.setOnAction((e)->{
			clientGraphique.loadScene(1);
		});
	}

	@Override
	public void loadScene() {
		primaryStage.setScene(scene);
	}
}
